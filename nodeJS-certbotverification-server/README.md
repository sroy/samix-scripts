This node server runs on port 80 in order to listen for CertBot's verification.

Replace the endpoint and the res.send string with the one provided by CertBot on the command line after running:

certbot certonly --manual
---
To install certbot, copy-paste those lines in a terminal :
$ sudo add-apt-repository ppa:certbot/certbot
$ sudo apt-get update
$ sudo apt-get install certbot

