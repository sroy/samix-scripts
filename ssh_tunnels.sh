#!/bin/bash
#
# Check ssh tunnels at anytime
echo "# CHECK TUNNELS (root user required)"
sudo lsof -i -n | egrep '\<ssh\>'
echo "# SHOWING CONNETIONS"
sudo lsof -i -n | egrep '\<sshd\>'
