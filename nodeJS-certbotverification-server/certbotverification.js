import { createRequire } from 'module';
const require = createRequire(import.meta.url);
const express = require('express');

// const express = require('express');
const app = express();

    // Verify certbot
app.get('/.well-known/acme-challenge/szKBCyCA3U1cr6mXR7_UoPqEx7FgIYAHo4vbpSRjYGU', (req, res) => {
  res.send('szKBCyCA3U1cr6mXR7_UoPqEx7FgIYAHo4vbpSRjYGU.K3YpnGurRUPeVFoy-RsPXB3l8Aw9C_62Q91RL8-tcow');
});

app.listen(80, () => console.log('\n### Waiting to verify on port 80.\nKILL PROCESS WHEN COMPLETED'));