#!/bin/bash
# Personal script to fix permissions and clear Grav cache from CLI
STRING="Running Grav Checkup"
echo $STRING
sudo chown -R sam:www-data public_html
sudo chmod -R 775 public_html
cd public_html
sudo ./bin/grav clear-cache
STRING="Complete"
echo $STRING
