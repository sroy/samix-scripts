#
# grep -rnw '/path/to/somewhere/' -e "pattern"

# -r or -R is recursive,
# -n is line number, and
# -w stands match the whole word.
# -l (lower-case L) can be added to just give the file name of matching files.
# Along with these, --exclude or --include parameter could be used for efficient searching. Something like below:
