#!/bin/bash
#
# Save it inside /home/$USER/bin and make it executable using:
# sudo chmod +x /home/$USER/bin/public_html_fix.sh
# public_html_fix.sh
sudo adduser $USER www-data
sudo chown -R www-data:www-data /home/$USER/public_html
sudo chmod -R 775 /home/$USER/public_html
